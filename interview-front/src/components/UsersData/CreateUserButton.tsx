import React, { useState } from "react";
import {
  Box,
  Grid,
  Button,
  Typography,
  FormControl,
  InputLabel,
  OutlinedInput,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  IconButton,
  Select,
  MenuItem,
  Snackbar,
  useTheme,
  makeStyles } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { Close } from "@material-ui/icons";
import axios from "axios";

export interface ICreateUserButton {
  getUsers: () => void;
}

const CreateUserButton = ({ getUsers }: ICreateUserButton) => {
  const [open, setOpen] = useState(false);
  const [snackbar, setSnackbar] = useState<{
    type: "success" | "info" | "warning" | "error",
    message: string, time?: number } | null>(null);

  const theme = useTheme();
  const useStyles = makeStyles({
    root: {
      maxWidth: 900,
      padding: theme.spacing(4)
    },
    form: {
      padding: theme.spacing(4)
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(4),
      top: theme.spacing(4),
      color: theme.palette.error.main,
    }
  });
  const classes = useStyles();

  const onOpenRegisterDialog = () => setOpen(true);
  const onCloseRegisterDialog = () => setOpen(false);

  const onRegister = (event: any) => {
    event.preventDefault();
    const {
      name,
      mail,
      pass,
      role,
      phone } = event.target.elements;

    const id = new Date().getTime();
    const nameValue = name.value.trim();
    const mailValue = mail.value.trim();
    const passValue = pass.value.trim();
    const roleValue = role.value.trim();
    const phoneValue = phone.value.trim();

    // eslint-disable-next-line
    const mailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const validEmail = mailRegex.test(mailValue.toLowerCase());
    
    if (validEmail) {
      const newData = {
        id,
        name: nameValue,
        mail: mailValue,
        pass: passValue,
        role: roleValue,
        phone: phoneValue
      }
      
      axios.post("//localhost:3001/", { newData })
        .then(() => {
          getUsers();
          setOpen(false);
        });
    } else setSnackbar({ type:"error", message: "No es un correo válido" });
  }

  return <Box>
    <Button color="primary" variant="contained" onClick={onOpenRegisterDialog}>Crear</Button>
    <Dialog
      open={open}
      classes={{ paper: classes.root }}
      onClose={onCloseRegisterDialog}
      aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title" disableTypography>
        <Typography variant="h6">Agregar nuevo usuario</Typography>
        <IconButton aria-label="close" className={classes.closeButton} onClick={onCloseRegisterDialog}>
          <Close />
        </IconButton>
      </DialogTitle>
      <form onSubmit={onRegister} className={classes.form}>
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <FormControl fullWidth variant="outlined">
                <InputLabel htmlFor="input-names">Nombre</InputLabel>
                <OutlinedInput
                  required
                  id="input-names"
                  type="text"
                  name="name"
                  label="Nombre"/>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormControl fullWidth variant="outlined">
                <InputLabel htmlFor="input-email">Correo</InputLabel>
                <OutlinedInput
                  required
                  id="input-email"
                  type="text"
                  name="mail"
                  label="Correo"/>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormControl fullWidth variant="outlined">
                <InputLabel htmlFor="input-password">Contraseña</InputLabel>
                <OutlinedInput
                  required
                  id="input-password"
                  type="password"
                  name="pass"
                  label="Contraseña"
                  autoComplete="new-password"/>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormControl fullWidth variant="outlined">
                <InputLabel htmlFor="input-role">Rol asociado</InputLabel>
                <Select
                  required
                  labelId="input-role"
                  name="role"
                  label="Rol asociado">
                  <MenuItem value={"admin"}>Administrador</MenuItem>
                  <MenuItem value={"user"}>Usuario</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormControl fullWidth variant="outlined">
                <InputLabel htmlFor="input-phone">Teléfono</InputLabel>
                <OutlinedInput
                  required
                  id="input-phone"
                  type="number"
                  name="phone"
                  label="Teléfono"/>
              </FormControl>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions disableSpacing>
          <Button
            variant="contained"
            type="submit"
            color="secondary"
            style={{
              minWidth: 150,
              marginRight: 8
            }}>Aceptar
          </Button>
          <Button
            variant="outlined"
            onClick={onCloseRegisterDialog}
            color="secondary"
            style={{
              minWidth: 150,
              marginLeft: 8
            }}>Cancelar
          </Button>
        </DialogActions>
      </form>
    </Dialog>
    <Snackbar
      open={snackbar !== null}
      autoHideDuration={snackbar?.time ?? 6000}
      onClose={() => setSnackbar(null)}>
      <Alert onClose={() => setSnackbar(null)} severity={snackbar?.type}>
        {snackbar?.message}
      </Alert>
    </Snackbar>
  </Box>
}

export default CreateUserButton;