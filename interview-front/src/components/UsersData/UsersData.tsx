import React, { useState, useEffect, useContext } from "react";
import {
  Box,
  Paper,
  Typography,
  Snackbar,
  makeStyles,
  useTheme } from "@material-ui/core";
import { Group } from "@material-ui/icons";
import { Alert } from "@material-ui/lab";

import UsersTable from "../UsersData/UsersTable";
import CreateUserButton from "./CreateUserButton";
import { AuthContext } from "../Tools/Auth";
import axios from "axios";

export interface IUserData {
  id?: string;
  name: string;
  mail: string;
  role: "admin" | "user";
  phone: number;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    overflow: "auto"
  },
  titleTable: {
    display: 'flex',
    alignItems: "center",
    flexGrow: 1,
    paddingBottom: theme.spacing(2)
  },
  titleIcon: {
    fontSize: 30,
    paddingRight: theme.spacing(2)
  },
  titleText: {
    [theme.breakpoints.down('sm')]: {
      display: "none"
    },
  }
}))

const UsersData = () => {
  const [users, setUsers] = useState<IUserData[] | undefined>(undefined);
  const [canEdit, setCanEdit] = useState<boolean>(false);
  const [snackbar, setSnackbar] = useState<{
    type: "success" | "info" | "warning" | "error",
    message: string, time?: number } | null>(null);

  const { currentRole } = useContext(AuthContext);
    
  const theme = useTheme();
  const classes = useStyles(theme);

  useEffect(() => {
    getUsers();
  }, []);

  useEffect(() => {
    if (currentRole === "admin") setCanEdit(true);
    else setCanEdit(false);
  }, [currentRole]);

  const getUsers = () => axios.get("//localhost:3001/")
    .then(response => setUsers(response.data));

  return <React.Fragment>
    <Box px={2} height="calc(100% - 48px)">
      <Paper className={classes.paper}>
        <Box className={classes.titleTable}>
          <Box
            px={2}
            display="flex"
            alignItems="center"
            flexGrow={1}>
            <Group color="primary" className={classes.titleIcon}/>
            <Typography
              className={classes.titleText}
              variant="h6"
              color="primary"
              component="p">Usuarios existentes
            </Typography>
          </Box>
          {canEdit && <CreateUserButton getUsers={getUsers}/>}
        </Box>
        <UsersTable
          users={users}
          canEdit={canEdit}
          getUsers={getUsers}/>
      </Paper>
    </Box>
    <Snackbar
      open={snackbar !== null}
      autoHideDuration={snackbar?.time ?? 6000}
      onClose={() => setSnackbar(null)}>
      <Alert onClose={() => setSnackbar(null)} severity={snackbar?.type}>
        {snackbar?.message}
      </Alert>
    </Snackbar>
  </React.Fragment> 
}

export default UsersData;