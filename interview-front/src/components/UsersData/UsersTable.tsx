import React, { forwardRef, useState } from "react";
import MaterialTable, { Icons } from "material-table";
import { Snackbar, Box } from "@material-ui/core";
import { Alert } from "@material-ui/lab";

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

import { IUserData } from "./UsersData";
import axios from "axios";

const MaterialTableIcons: Icons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} color="primary" ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

export interface IUsersTable {
  users: IUserData[] | undefined;
  canEdit: boolean;
  getUsers: () => void;
}

const UsersTable = ({ users, canEdit, getUsers }: IUsersTable) => {
  const [snackbar, setSnackbar] = useState<{
    type: "success" | "info" | "warning" | "error",
    message: string, time?: number } | null>(null);

  if (users) return <Box>
    <MaterialTable
      style={{
        tableLayout: "fixed",
        whiteSpace: "nowrap",
        boxShadow: "none",
        MozBoxShadow: "none",
        WebkitBoxShadow: "none",
      }}
      options={{
        filtering: true,
        actionsColumnIndex: 7,
        exportButton: canEdit,
        exportFileName: "tabla_de_usuarios"
      }}
      icons={MaterialTableIcons}
      columns={[
        { title: "Nombre", field: "name" },
        { title: "Correo", field: "mail" },
        { title: "Rol asociado", field: "role", lookup: {
          "admin": "Administrador",
          "user": "Usuario" } },
        { title: "Teléfono", field: "phone" }
      ]}
      data={users}
      title=""
      localization={{
        body: {
          editRow: {
            deleteText: '¿Desea eliminar este usuario?',
            cancelTooltip: "Cancelar",
            saveTooltip: 'Guardar',
          },
          editTooltip: "Editar usuario",
          deleteTooltip: "Eliminar usuario"
        },
        header: {
          actions: "Acciones",
        }
      }}
      editable={canEdit ? {
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {            
              if (isNaN(Number(newData.phone))) {
                setSnackbar({ type:"error", message: "Teléfono invalido" });
                reject();
                return
              }
              
              axios.put("//localhost:3001/", { oldData, newData }).then((response) => {                               
                getUsers();
                resolve('');
              }).catch(() => {
                setSnackbar({ type:"error", message: "Algo ha salido mál editando el usuario" });
                reject();
                return
              });
            }, 1000)
          }),
        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              axios.delete(`//localhost:3001/${oldData.id}`).then((response) => {                
                console.log(response);
                getUsers();
                resolve('');
              }).catch(() => {
                setSnackbar({ type:"error", message: "Algo ha salido mál eliminando el usuario" });
                reject();
                return
              });
            }, 1000)
          }),
      } : undefined}/>
    <Snackbar
      open={snackbar !== null}
      autoHideDuration={snackbar?.time ?? 6000}
      onClose={() => setSnackbar(null)}>
      <Alert onClose={() => setSnackbar(null)} severity={snackbar?.type}>
        {snackbar?.message}
      </Alert>
    </Snackbar>
  </Box>
  else return null;
}

export default UsersTable;