import React, { useState, useContext, ChangeEvent } from "react";
import { Redirect } from "react-router-dom";
import {
  Paper,
  Typography,
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  Button,
  useTheme,
  makeStyles,
  Box,
  Divider,
  Snackbar } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { PersonOutlineOutlined, LockOutlined } from "@material-ui/icons"
import axios from "axios";

import { AuthContext } from "./Tools/Auth";
import Loader from "./Tools/Loader";

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: 20,
    MozBorderRadius: 20,
    WebkitBorderRadius: 20,
    padding: theme.spacing(6)
  },
  input: {
    "&&&:before": {
      borderBottom: "none"
    },
    "&&:after": {
      borderBottom: "none"
    }
  }
}));

const Login = () => {
  const [mail, setMail] = useState('');
  const [pass, setPass] = useState('');
  const [loading, setLoading] = useState(false);
  const [snackbar, setSnackbar] = useState<{
    type: "success" | "info" | "warning" | "error",
    message: string, time?: number } | null>(null);

  const theme = useTheme();
  const classes = useStyles(theme);

  const onLoginWithMail = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    setLoading(true);

    // eslint-disable-next-line
    const mailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const validEmail = mailRegex.test(mail.toLowerCase().trim());

    if (validEmail)
      axios.post("//localhost:3001/auth", { mail, pass })
        .then(response => {
          if (response.data.code > -1) {            
            window.location.href = "/dashboard";
          }
          else {
            setSnackbar({ type:"error", message: response.data.message });
            setLoading(false);
          }
          
          setLoading(false);
        })
        .catch((error: Error) => {
          setSnackbar({ type:"error", message: error.message });
          setLoading(false);
        });
    else setSnackbar({ type:"error", message: "No es un correo válido" });
  }

  const onChangeEmail = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    event.preventDefault();
    setMail(event.target.value.trim());
  }

  const onChangePassword = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    event.preventDefault();
    setPass(event.target.value.trim());
  }

  const { currentUser } = useContext(AuthContext);

  if (currentUser) return <Redirect to="/dashboard" />;
  else return <Paper className={classes.root} elevation={6}>
    {loading && <Loader message="Estamos preparando todo para tí"/>}
    <Typography align="center" variant="h4" component="h2">Inicio de sesión</Typography>
    <Box my={5}>
      <Box boxShadow={5} borderRadius={0}>
        <Box p={1}>
          <FormControl fullWidth>
            <InputLabel htmlFor="input-email">Correo</InputLabel>
            <Input
              id="input-email"
              className={classes.input}
              type="text"
              value={mail}
              onChange={onChangeEmail}
              endAdornment={<InputAdornment position="end">
                <PersonOutlineOutlined/>
              </InputAdornment>}
            />
          </FormControl>
        </Box>
        <Divider color="#f0f0f0"/>
        <Box p={1}>
          <FormControl fullWidth>
            <InputLabel htmlFor="input-password">Contraseña</InputLabel>
            <Input
              id="input-password"
              className={classes.input}
              fullWidth
              type="password"
              value={pass}
              onChange={onChangePassword}
              endAdornment={<InputAdornment position="end">
                <LockOutlined/>
              </InputAdornment>}
            />
          </FormControl>
        </Box>
      </Box>
    </Box>
    <Box mb={1} mt={4}>
      <Button
        onClick={onLoginWithMail}
        variant="contained"
        color="primary"
        fullWidth>Iniciar sesión</Button>
    </Box>
    <Snackbar
      open={snackbar !== null}
      autoHideDuration={snackbar?.time ?? 6000}
      onClose={() => setSnackbar(null)}>
      <Alert onClose={() => setSnackbar(null)} severity={snackbar?.type}>
        {snackbar?.message}
      </Alert>
    </Snackbar>
  </Paper>
}

export default Login;