import axios from "axios";
import React, { useEffect, useState, createContext, ReactElement } from "react";
import Loader from "./Loader";

export const AuthContext = createContext<{
  currentUser: string | null,
  currentRole: string | null }>({
    currentUser: null,
    currentRole: null
  });

export interface IAuthProvider {
  children: ReactElement;
}

export const AuthProvider = ({ children }: IAuthProvider) => {
  const [currentUser, setCurrentUser] = useState<string | null>(null);
  const [currentRole, setCurrentRole] = useState<string | null>(null);
  const [pending, setPending] = useState(true);

  useEffect(() => {
    axios.get('//localhost:3001/auth').then(response => {
      console.log(response.data);
      setCurrentUser(response.data.mail);
      setCurrentRole(response.data.role)
      setPending(false)
    });
  }, []);

  if(pending){
    return <Loader message="Cargando..."/>
  }

  return (
    <AuthContext.Provider value={{ currentUser, currentRole }}>
      {children}</AuthContext.Provider>
  );
}

export default AuthProvider;