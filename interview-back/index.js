const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const e = require('express');
// const session = require('express-session');
const app = express();

app.use(cors());
app.use(cookieParser());
/* app.use(session({
  cookieName: 'session',
	secret: 'secret-key',
	resave: true,
	saveUninitialized: true,
  cookie: {
    sameSite: true,
    secure: false,
    expires: false,
    httpOnly: false,
  }
})); */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

let users = [{
	id: 1,
  name: "Sergio Morales",
	mail: "sergio@m.com",
	pass: "123",
	role: "admin",
  phone: "12345"
}, {
	id: 2,
  name: "Alberto Usma",
	mail: "alberto@m.com",
	pass: "123",
	role: "user",
  phone: "67890"
}];

let session = {
  mail: null,
  role: null
}


// eslint-disable-next-line
const mailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// const validEmail = mailRegex.test(newEmail.toLowerCase());
/*
const dataUpdate = [...users];
const index = dataUpdate.findIndex((data) =>
  data.email === oldData!.email);

const email = oldData!.email;
const newEmail = newData.email;
dataUpdate[index] = newData;
*/

app.get('/', (req, res) => res.send(users));
app.put('/', (req, res) => {
	if (req.body.oldData && req.body.newData) {
		users = [...users.reduce((acc, el) => {
			if (el.id === req.body.oldData.id)
				acc.push({
          id: req.body.newData.id,
          name: req.body.newData.name,
          mail: req.body.newData.mail,
          pass: req.body.newData.pass,
          role: req.body.newData.role,
          phone: req.body.newData.phone,
        });
			else acc.push(el);
			return acc;
		}, [])];
		res.send({
			message: "Elemento modificado",
			code: 1
		});
	} else {
		res.send({
			message: "No fue posible modificar el elemento",
			code: -1
		});
	}
	res.end();
});
app.post('/', (req, res) => {
	if (req.body.newData) {
		users.push({ id: Number(req.body.newData.id), ...req.body.newData });
		res.send({
			message: "Elemento agregado",
			code: 1
		});
	} else res.send({
		message: "No se puede agregar el elemento",
		code: -1
	});

	res.end();
});
app.delete('/:id', (req, res) => {
	let deleted = false;
	users = [...users.reduce((acc, el) => {
		if (el.id === Number(req.params.id)) deleted = true;
		else acc.push(el);
		return acc;
	}, [])];

	if (deleted)
		res.send({
			message: "Elemento eliminado",
			code: 1
		});
	else res.send({
		message: "Elemento no encontrado",
		code: -1
	});	

	res.end();
});

app.post('/auth', (req, res) => {
	const mail = req.body.mail;
	const pass = req.body.pass;

	if (mail.length > 0 && pass.length > 0) {
		const correctData = users.reduce((acc, el) => {
			if (el.mail === mail && el.pass === pass)
				return {
          mail: el.mail,
          role: el.role
        };
			else return acc;
		}, {
      mail: null,
      role: null
    });
		
		if (correctData.mail) {
			session.mail = correctData.mail;
			session.role = correctData.role;
      
			res.send({
				message: "Ok.",
				code: 1
			});
		} else res.send({
      message: "Nombre de usuario y/o contraseña incorrectos",
      code: -1
    });

		res.end();
	} else {
		res.send({
			message: "Porfavor ingrese nombre de usuario y contraseña",
			code: -1
		});
		res.end();
	}
});

app.get('/auth', (req, res) => {
	if (session.mail) {
		res.send(session);
	} else res.send('Debes loguear!');
	res.end();
});

app.get('/logout', (req, res) => {
	session.mail = false;
  session.role = null;
	res.send({
    message: "Ok.",
    code: 1
  });
	res.end();
});

app.listen(3001);